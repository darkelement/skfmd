#!/usr/bin/python2

import fileinput
import pylab

#f = fileinput.FileInput('results/s_7_4-11_4000_3000.dat')
f = fileinput.FileInput('results/s_6_1-15_10000_2000.dat')

for i in (1, 2):
	for line in f:
		if line == "# heating\n":
			break

tab = {}
for line in f:
	if line == "\n":
		break
	ll = line.split('\t')
	tab[float(ll[0])] = float(ll[3])

p = zip(tab.keys(), tab.values())
p.sort()
x = [i[0] for i in p]
y = [i[1] for i in p]

h = 5
xderiv = [0 for i in x]
yderiv = [0 for i in y]
for i in range(2*h, len(x)-2*h):
	xderiv[i] = (x[i+2] + x[i-2]) / 2.0
	#yderiv[i] = (y[i+2] - y[i-2]) / (x[i+2] - x[i-2])
	yderiv[i] = (y[i-2*h] - 8*y[i-h] + 8*y[i+h] - y[i+2*h]) / (x[i+h] - x[i-h]) * 2*h / 12.0


pylab.plot(x, y, 'o')
pylab.plot(xderiv[2*h:-2*h], yderiv[2*h:-2*h], '.', ls='-')
pylab.grid()
pylab.savefig('heat.png')
