ifeq "${NODEBUG}" "true"
	debug_flags =
else
	debug_flags = -g -pg
endif

define_flags =
ifeq "${NOGUI}" "true"
	define_flags += -DNOGUI
endif
ifeq "${NOPLOTS}" "true"
	define_flags += -DNOPLOTS
endif

CC = gcc -Wall ${debug_flags}
PC = pkg-config --libs --cflags

skfmd_deps = Makefile src/tools.h src/tools.c src/verlet.h src/verlet.c src/physics.h \
             src/physics.c src/simulation.h src/simulation.c src/skfmd.c
ifneq "${NOGUI}" "true"
	skfmd_deps += src/gui.h src/gui.c
endif

skfmd_source = src/tools.c src/verlet.c src/physics.c src/simulation.c src/skfmd.c
ifneq "${NOGUI}" "true"
	skfmd_source += src/gui.c
endif

math_flags = -lm
gsl_flags = `${PC} gsl`
glib_flags = `${PC} glib-2.0`
gtk_flags = `${PC} gtk+-2.0`
gtkglext_flags = `${PC} gtkglext-1.0`
gtkextra_flags = `${PC} gtkextra-2.0`
glut_flags = -lglut
openmp_flags = -fopenmp

lib_flags=${math_flags} ${gsl_flags} ${glib_flags} ${openmp_flags}
ifneq "${NOGUI}" "true"
	lib_flags += ${gtk_flags} ${gtkglext_flags} ${glut_flags}
	ifneq "${NOPLOTS}" "true"
		 lib_flags += ${gtkextra_flags}
	endif
endif

all: skfmd report.pdf

program: skfmd
skfmd: ${skfmd_deps}
	${CC} ${skfmd_source} -o skfmd ${lib_flags} ${define_flags}

report.pdf: report/report.tex plot.sh plot.py
	./plot.sh results/s_6_1-15_10000_2000.dat -o report/s_6_1-15_10000_2000.eps
	./plot.sh results/r_6_1-20_10000.dat -o report/r_6_1-20_10000.eps
	./plot.sh results/g_4.dat -o report/g_4.eps
	./plot.sh results/s_7_4-11_4000_3000.dat results/s_6_1-15_10000_2000.dat results/s_5_3-9_4000_2000.dat -o report/scaling.eps
	./plot.py
	mv heat.png report/heat.png

	cd report; pdflatex report.tex; pdflatex report.tex
	cp report/report.pdf ./

clear:
	rm -f skfmd report/report.{aux,pdf,log} report/*{.pdf,.eps} report/heat.png results/*.eps report*.pdf gmon.out *.eps *.deb

dist: skfmd-1.0-1-i686.deb

skfmd-1.0-1-i686.deb: skfmd dist/debian/control
	mkdir -p build/DEBIAN build/usr/bin
	cp dist/debian/control build/DEBIAN/
	cp skfmd build/usr/bin
	fakeroot dpkg-deb --build build
	mv build.deb skfmd-1.0-1-i686.deb
	rm -Rf build
