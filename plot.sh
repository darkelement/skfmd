#!/bin/bash

#linetype=linespoints
linetype=points

sources=()

while test "$1"; do
	case $1 in
		-o|--output)
			shift
			output="$1"
		;;
		*)
			if ! test -r "$1"; then
				echo "File '$1' not found"
				exit 1
			fi
			sources+=("$1")
		;;
	esac
	shift
done

if test "${#sources[@]}" = "0"; then
	echo "No sources specified!"
	exit 2
fi

if test "$output" = ""; then
	if test "${#sources[@]}" = "1"; then
		output=$sources.eps
	else
		output=output.eps
	fi
fi

plot=
for source in ${sources[@]}; do
	firstline=`cat "$source" | head -n 1`
	mode=${firstline:9}
	L=`cat "$source" | grep "# L = " | tail --bytes=-2`
	N=$(($L*$L*$L + 3*$L*($L-1)*($L-1)))

	if test "$plot" != ""; then
		plot+=", "
	fi

	if test "$mode" = "scaling"; then

plot+=$(cat <<EOF
      "$source" index 2 using 1:3 title "N=$N,\tpotencjalna" with $linetype, \
      "$source" index 2 using 1:4 title "N=$N,\tcałkowita" with $linetype
EOF
)
#      "$source" index 0 using 1:3 title "h1 pot" with $linetype lt 7 pt 1, \
#      "$source" index 1 using 1:3 title " c pot" with $linetype lt 3 pt 1, \
#      "$source" index 0 using 1:4 title "h1 tot" with $linetype lt 7 pt 2, \
#      "$source" index 1 using 1:4 title " c tot" with $linetype lt 3 pt 2, \

	elif test "$mode" = "setting"; then

plot+=$(cat <<EOF
      "$source" using 1:3 title "potencjalna" with $linetype, \
      "$source" using 1:4 title "całkowita" with $linetype
EOF
)

	elif test "$mode" = "measuring"; then

plot+=$(cat <<EOF
      "$source" using 4 title "całkowita" with $linetype
EOF
)

	elif test "$mode" = "gui"; then

plot+=$(cat <<EOF
      "$source" using 1:3 title "potencjalna" with $linetype, \
      "$source" using 1:4 title "całkowita" with $linetype
EOF
)

	else
		echo "Unknown mode"
		exit 1
	fi
done

gnuplot << EOF
set term postscript enhanced
set size 1.3, 1.3
set encoding iso_8859_2

set xlabel "Temperatura"
set ylabel "Energia"
set key right bottom

set output "$output"
plot $plot
EOF

exit 0
