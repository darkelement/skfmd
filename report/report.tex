\documentclass[a4paper,11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[polish]{babel}
\usepackage[T1]{fontenc}

\usepackage{geometry}
\usepackage[pdftex]{color,graphicx}

\title{Zależność energii i ciepła właściwego od temperatury w trójwymiarowym klastrze}
\author{Wojciech Kluczka\footnote{\texttt{wojtekkluczka@gmail.com}}}
\date{\today}

\begin{document}

\maketitle

\begin{abstract}
Niniejsza praca przedstawia wyniki pomiarów zachowania się energii potencjalnej, całkowitej i ciepła właściwego w symulacji komputerowej na trójwymiarowym klastrze atomów oddziałujących potencjałem Lenarda-Jonesa.
\end{abstract}

\newpage

\section{Wstęp}

Celem projektu było wyznaczenie energii i ciepła właściwego od temperatury w trójwymiarowym klastrze atomów oddziałujących potencjałem Lenarda-Jonesa
\begin{equation}
U(\vec{r}) = 4\varepsilon \left(\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^6\right).
\end{equation}
Składowe siły w takim potencjale wyrażają się wzorem:
\begin{equation}
F_i(\vec{r}) = -\frac{\mathrm{d}}{{\mathrm{d}r_i}} U(\vec{r}) =
24\varepsilon \left(\sigma^6r^{-8} - \sigma^{12}r^{-14}\right),
\end{equation}
gdzie $\vec{r} = (r_x, r_y, r_z)$ i $r = \|\vec{r}\| = \sqrt{r_x^2 + r_y^2 + r_z^2}$.

Atomy umieszczane są początkowo w sieci FCC w minimach potencjału Lenarda-Jonesa (sytuacja przedstawiona na rysunku [\ref{fig:r0}]). Atomy dążą do konfiguracji, w której mają największą liczbę sąsiadów, więc dla małych układów takie ułożenie nie jest optymalne i układ przechodzi do stan o mniejszej energii potencjalnej (rysunek [\ref{fig:r}]).

\begin{figure}[hb]
 \begin{center}
  \includegraphics[scale=0.5]{r0.png}
 \end{center}
 \caption{Początkowe ustawienie atomów w sieci FCC (stan metastabilny) ($N=172$)}
 \label{fig:r0}
\end{figure}
\begin{figure}[ht]
 \begin{center}
  \includegraphics[scale=0.30]{r1.png}
  \includegraphics[scale=0.31]{r2.png}
 \end{center}
 \caption{Koncentryczne ustawienie atomów w stanie stabilnym ($N=172$)}
 \label{fig:r}
\end{figure}

Jako jednostkę energii używać będziemy $\varepsilon$, a odległości $\sigma$. Za jednostkę temperatury przyjmujemy $\frac{\varepsilon}{[k_B]}$, gdzie $k_B$ to stała Boltzmana. Dla uproszczenia rozważań i przyspieszenia symulacji kładziemy $\varepsilon = \sigma = k_B = 1$.

\section{Oprogramowanie}

Do symulacji użyto algorytmu Verleta.

Program pracuje w trzech trybach:
\begin{enumerate}
\item \textbf{Skalowanie prędkości} -- program $s$-krotnie skaluje prędkości o czynnik $S$ z $w$ krokami między skalowaniami. Następnie układ przez $m$ kroków jest doprowadzany do stanu równowagi i przez $m$ kroków mierzone są wielkości fizyczne. Kroki są powtarzane do osiągnięcia zadanej temperatury.
\item \textbf{Ustawianie prędkości} -- z rozkładu Maxwella - dla każdej temperatury przygotowywany jest nowy układ z prędkościami losowanymi z rozkładu Maxwella. Następnie układ przez $m$ kroków jest doprowadzany do stanu równowagi i przez $m$ kroków mierzone są wielkości fizyczne.

W tej metodzie mamy do czynienia z niezerowym momentem pędu. Sposób jego usuwania został opisany w sekcji \ref{sec:angmomentum}.
\item \textbf{Nieprzerwane skalowanie} -- prędkości są skalowane bez przerwy w każdym kroku czasowym, wielkości fizyczne są mierzone w trakcie skalowania. \emph{(Uwaga: metoda niefizyczna z uwagi na to, że układ nie jest doprowadzany do stanu równowagi.)}
\end{enumerate}

Źródła programu dostępne są pod poniższym adresem: \newline \texttt{git://bitbucket.org/darkelement/skfmd.git}

\section{Usuwanie momentu pędu\label{sec:angmomentum}}

Znajdźmy taki wektor momentu pędu $\vec{m}$, że po odjęciu go od momentów pędu wszystkich cząstek całkowity moment pędu układu będzie równy zeru:
\begin{equation}
\sum\limits_{i=0}^N \left(\vec{m}_i-\vec{m}\right)=0 \qquad\Longrightarrow\qquad \vec{m} =
\frac{1}{N}\sum\limits_{i=0}^N \vec{m}_i = \frac{\vec{M}}{N}
\end{equation}
Stąd otrzymujemy równania na prędkości:
\begin{equation}
\varepsilon_{ijk}r_jv_k=\frac{M_i}{N}.
\end{equation}
Niestety nie da ich się w prosty sposób rozwiązać, jednak możemy otrzymać z nich sześć równań następującej postaci:
\begin{eqnarray}
v_1 = \frac{1}{r_2}\left(-\frac{M_3}{N} + r_1v_2\right), \qquad v_1 = \frac{1}{r_3}\left(+\frac{M_2}{N} + r_1v_3\right), \\
v_2 = \frac{1}{r_1}\left(+\frac{M_3}{N} + r_2v_1\right), \qquad v_2 = \frac{1}{r_3}\left(-\frac{M_1}{N} + r_2v_3\right), \\
v_3 = \frac{1}{r_1}\left(-\frac{M_2}{N} + r_3v_1\right), \qquad v_3 = \frac{1}{r_2}\left(+\frac{M_1}{N} + r_3v_2\right),
\end{eqnarray}
które łatwo możemy rozwiązać iteracyjnie.

\emph{Uwaga: metoda ta zachowuje się niestabilnie jeśli w układzie znajdują się cząstki o małych wartościach składowych położenia, dlatego można ją stosować tylko dla układu niezaburzonego.}

\section{Wyniki}

Na wykresach [\ref{fig:s_6_1-15_10000_2000}-\ref{fig:g_4}] przedstawione są zależności energii całkowitej i potencjalnej od temperatury.

\paragraph{Skalowanie} Na rysunku [\ref{fig:s_6_1-15_10000_2000}] możemy wyróżnić jedno przejście fazowe dla temperatur $0.5 - 0.7 \varepsilon$. Rysunek [\ref{fig:heat}] przedstawia wykresy energii potencjalnej i związanego z nią przez pochodną po temperaturze ciepła właściwego. Piki na wykresie tej drugiej wielkości wskazują przejścia fazowe (z powodu małego rozmiaru układu wyniki naznaczone są silnym wpływem błędów statystycznych).

Pochodna liczona jest lekko zmodyfikowanym wzorem pięciopunktowym. Standardowy wzór pięciopunktowy wygląda następująco:
\begin{equation}
f'(x_i) = \frac{1}{12h}\left(f(x_{i-2}) - 8f(x_{i-1}) + 8f(x_{i+1}) - f(x_{i+2})\right)
\end{equation}
gdzie zakładamy, że znamy wartości funkcji $f$ w punktach $x_i$ takich, że $\forall i\ x_{i+1} - x_i = h$. W naszym przypadku punkty $x_i$ nie są od siebie równo oddalone. Ponadto dane obarczone są dość dużymi błędami statystycznymi, więc bierzemy nie najbliższe punkty, lecz oddalone o 5 i 10 kroków. Zmodyfikowany wzór możemy zapisać następująco:
\begin{equation}
f'(x_i) = \frac{10}{12}\frac{f(x_{i-10}) - 8f(x_{i-5}) + 8f(x_{i+5}) - f(x_{i+10})}{x_{i+5} - x_{i-5}}.
\end{equation}

\begin{figure}
 \begin{center}
  \includegraphics[scale=0.39,angle=270]{s_6_1-15_10000_2000.eps}
 \end{center}
 \caption{Skalowanie: $N=666, m=10000, s=2000, w=10, S=1.0001$}
 \label{fig:s_6_1-15_10000_2000}
\end{figure}

\paragraph{Ustawianie prędkości} Na rysunku [\ref{fig:r_6_1-20_10000}] znów widoczne jest jedno przejście fazowe w temperaturach $0.5 - 0.6 \varepsilon$, tym razem trochę mniej widoczne. Dość silnie dają o sobie znać błędy statystyczne. Wynikają one z tego, że temperaturę układu ustawiamy losując prędkości z rozkładu Maxwella, jest to zatem proces probabilistyczny i może się zdarzyć, że temperatura układu nie odpowiada dokładnie żądanej.

\begin{figure}
 \begin{center}
  \includegraphics[scale=0.39,angle=270]{r_6_1-20_10000.eps}
 \end{center}
 \caption{Ustawianie: $N=666, m=10000, s=1000, w=10, S=1.0001$}
 \label{fig:r_6_1-20_10000}
\end{figure}

\paragraph{Nieprzerwane skalowanie prędkości} Mimo iż jest to metoda niefizyczna otrzymujemy zaskakująco dobre rezultaty (nawet dla małych układów). Na rysunku [\ref{fig:g_4}] możemy z łatwością odróżnić przejście miedzy fazą stałą i ciekłą (w temperaturze $4.2\varepsilon$) oraz ,,skoki'' energii związane z odrywaniem się cząstek.

\begin{figure}
 \begin{center}
  \includegraphics[scale=0.39,angle=270]{g_4.eps}
 \end{center}
 \caption{Nieprzerwane skalowanie: $N=172, S=1.0001$}
 \label{fig:g_4}
\end{figure}

Tylko w przypadku skalowania pomiary przeprowadzono do przekroczenia $0\varepsilon$ przez energię całkowitą. W tym momencie cząstki już ze sobą efektywnie nie oddziałują, i rzeczywiście obserwujemy \emph{plateau} na wykresie energii potencjalnej.

Na rysunku [\ref{fig:scaling}] zostało przedstawiona porównanie energii całkowitych i potencjalnych dla układów o 365, 666 i 1099 cząstkach. Widzimy, że dla dużych układów początek przejścia fazowego przesuwa się w stronę wyższych temperatur. Wynika to stąd, że w małych układach wszystkie cząstki znajdują się efektywnie na powierzchni, więc energia spójności na cząstkę jest mniejsza niż dużym klastrze.
\begin{figure}
 \begin{center}
  \includegraphics[scale=0.39,angle=270]{scaling.eps}
 \end{center}
 \caption{Porównanie wykresów energii całkowitej i potencjalnej układów o 365, 666 i 1099 cząstkach.}
 \label{fig:scaling}
\end{figure}

\begin{figure}
 \begin{center}
  \includegraphics[scale=0.55,angle=0]{heat.png}
 \end{center}
 \caption{Wykres ciepła właściwego i energii potencjalnej dla układu 666 cząstek.}
 \label{fig:heat}
\end{figure}

\section{Podsumowanie}

Można by spodziewać się otrzymania dwóch przejść fazowych: pomiędzy fazą stałą i ciekłą oraz ciekłą i gazową. Wykresy wskazują jednak na istnienie tylko jednego przejścia. Wynika to stąd, że cząstki o wysokiej energii odrywają się od klastra już w temperaturach niższych niż charakterystyczne dla przejścia ciecz--gaz, zatem dla tak małych układów (rzędu kilkuset atomów) liczba atomów związanych w klastrze jest zbyt mała by zaobserwować drugie przejście. Prawdopodobnie lepsze rezultaty dałoby badanie większych układów, jednak to wymaga dostępu do większych zasobów komputerowych.

Spośród dwóch ,,fizycznych'' metod trochę lepsze rezultaty dała metoda skalowania prędkości, jednak z punktu widzenia obliczeń wydajniejsza czasowo i łatwiejsza w użyciu jest metoda ustawiania prędkości z rozkładu Maxwella.

\end{document}
