#include "verlet.h"

Space *verlet_r(Space *r, Space *v, Space *f, double h, int N) {
	Space *result = space_new(N);
	int i;
	for (i = 0; i < N; ++i) {
		result[i]->x = r[i]->x + h*v[i]->x + h*h*f[i]->x/2.0;
		result[i]->y = r[i]->y + h*v[i]->y + h*h*f[i]->y/2.0;
		result[i]->z = r[i]->z + h*v[i]->z + h*h*f[i]->z/2.0;
	}
	return result;
}

Space *verlet_v(Space *v, Space *f0, Space *f1, double h, int N) {
	Space *result = space_new(N);
	int i;
	for (i = 0; i < N; ++i) {
		result[i]->x = v[i]->x + h*(f0[i]->x + f1[i]->x)/2.0;
		result[i]->y = v[i]->y + h*(f0[i]->y + f1[i]->y)/2.0;
		result[i]->z = v[i]->z + h*(f0[i]->z + f1[i]->z)/2.0;
	}
	return result;
}
