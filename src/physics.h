#ifndef __PHYSICS_H__
#define __PHYSICS_H__

#include "tools.h"

Space *force(Space *r, unsigned N);
double potential(Space *r, unsigned N);
double kinetic(Space *v, unsigned N);
double temperature(Space *v, unsigned N);
Vec angular_momentum(Space *r, Space *v, unsigned N);
Vec mean_velocity(Space *v, unsigned N);

Histogram *correlation(Space *r, unsigned N);

#endif
