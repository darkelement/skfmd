/**
***  Verlet algorithm
**/

#ifndef __VERLET_H__
#define __VERLET_H__

#include "tools.h"

Space *verlet_r(Space *r, Space *v,  Space *f,  double, int);
Space *verlet_v(Space *v, Space *f0, Space *f1, double, int);

#endif
