#include "simulation.h"

#ifndef NOGUI
	#include "gui.h"
#endif

#include <math.h>
#include <glib.h>

#define SUCCESS 0
#define ERROR_PARSE 1
#define ERROR_GL 2

// Default values of flags
gboolean v = FALSE;
int L = 4;
double h = 0.01;
int nsm = 10000; // Number of steps to take average of measured quantities
int nss = 1000; // Number of steps to take while changing temperature
int nsw = 10; // Number of steps between scaling velocities
double ti = 0.01;
double tf = 1.00;
double ts = 0.01;
double mdisp = 0.0;
double scale = 1.0001;

#ifndef NOGUI
	char *mode = "gui";
#else
	char *mode = "setting";
#endif

static GOptionEntry entries[] = {
	{"version", 'v', 0, G_OPTION_ARG_NONE, &v, "print version and exit", NULL},
	{"length", 'L', 0, G_OPTION_ARG_INT, &L, "length (L^3 + 3*L*(L-1)^2 = number of particles [FCC])", "[3]"},
	{"step", 'h', 0, G_OPTION_ARG_DOUBLE, &h, "step", "[0.01]"},
	{"nsm", 'm', 0, G_OPTION_ARG_INT, &nsm, "number of steps to take average of measured quantities", "[10000]"},
	{"nss", 's', 0, G_OPTION_ARG_INT, &nss, "number of steps to take while changing temperature", "[1000]"},
	{"nsw", 'w', 0, G_OPTION_ARG_INT, &nsw, "number of steps between scaling velocities", "[10]"},
	{"temp-init", 't', 0, G_OPTION_ARG_DOUBLE, &ti, "Initial temperature", "[0.01]"},
	{"temp-final", 'T', 0, G_OPTION_ARG_DOUBLE, &tf, "Final temperature", "[1.00]"},
	{"temp-step", 'b', 0, G_OPTION_ARG_DOUBLE, &ts, "Temperature step (only in 'setting' mode)", "[0.01]"},
	{"mdisp", 'd', 0, G_OPTION_ARG_DOUBLE, &mdisp, "mean initial displacement of atoms", "[0.0]"},
	{"scale", 'S', 0, G_OPTION_ARG_DOUBLE, &scale, "scaling multiplier", "[1.0001]"},
	{"mode", 'M', 0, G_OPTION_ARG_STRING, &mode, "mode", "[scaling|setting|measuring|gui]"},
	{NULL}
};

int main(int argc, char *argv[]) {
	GError *error = NULL;
	GOptionContext *context;

	// Parsing options
	context = g_option_context_new("- molecular dynamics simulation");
	g_option_context_add_main_entries(context, entries, NULL);

	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		printf("Option parsing failed: %s\n", error->message);
		return ERROR_PARSE;
	}

	if (v) {
		printf("Vesion: 0.1.alpha\nAuthor: Wojciech Kluczka <wojtekkluczka@gmail.com>\n"
		       "Source: $ git clone https://bitbucket.org/darkelement/skfmd.git\n");
		return SUCCESS;
	}

	printf("# Mode = %s\n# L = %d\n# h = %f\n# nsm = %d\n# nss = %d\n"
	       "# nsw = %d\n# mdisp = %f\n# scale = %f\n# ti = %f\n# tf = %f\n# ts = %f\n\n",
	       mode, L, h, nsm, nss, nsw, mdisp, scale, ti, tf, ts); fflush(stdout);

	// Running simulation
	Simulation *sim = simulation_init(abs(L), fabs(h), abs(nsm), abs(nss), abs(nsw),
	                                  mdisp, fabs(scale), fabs(ti), fabs(tf), fabs(ts));
	if (g_strcmp0(mode, "scaling") == 0) {
		simulation_go_scaling(sim);
	} else if (g_strcmp0(mode, "setting") == 0) {
		simulation_go_setting(sim);
	} else if (g_strcmp0(mode, "measuring") == 0) {
		simulation_go_measuring(sim);
	}

#ifndef NOGUI
	else if (g_strcmp0(mode, "gui") == 0) {
		gtk_init(&argc, &argv);
		if (!gtk_gl_init_check(&argc, &argv)) {
			printf("YOUR SYSTEM DOES NOT SUPPORT GL!");
			simulation_free(sim);
			return ERROR_GL;
		}
		glutInit(&argc, argv);
		gui_build(sim);
		gtk_main();
		gui_destroy();
	}
#endif

	else {
		printf("Mode not found. Maybe program is compiled without support of this flag?\n");
	}

	simulation_free(sim);
	return SUCCESS;
}
