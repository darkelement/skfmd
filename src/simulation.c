#include "simulation.h"
#include "physics.h"
#include "verlet.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include <gsl/gsl_randist.h>

// Random displacement
#define RD gsl_ran_gaussian(self->rand, self->mean_displacement)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void simulation_normalize_velocities(Simulation *self) {
	int i;
	Vec mv = mean_velocity(self->v, self->N);
	for (i = 0; i < self->N; ++i) {
		self->v[i]->x -= mv.x;
		self->v[i]->y -= mv.y;
		self->v[i]->z -= mv.z;
	}
}

static void simulation_normalize_anagular_momentum_(Simulation *self) {
	int i;
	double _N = 1.0 / self->N;
	Vec M = angular_momentum(self->r, self->v, self->N);
	for (i = 0; i < self->N; ++i) {
		if (self->r[i]->x) {
			self->v[i]->y -= 0.5 * ( M.z * _N /*+ self->r[i]->y * self->v[i]->z*/) / self->r[i]->x;
			self->v[i]->z -= 0.5 * (-M.y * _N /*+ self->r[i]->z * self->v[i]->z*/) / self->r[i]->x;
		}
		if (self->r[i]->y) {
			self->v[i]->x -= 0.5 * (-M.z * _N /*+ self->r[i]->x * self->v[i]->y*/) / self->r[i]->y;
			self->v[i]->z -= 0.5 * ( M.x * _N /*+ self->r[i]->z * self->v[i]->y*/) / self->r[i]->y;
		}
		if (self->r[i]->z) {
			self->v[i]->x -= 0.5 * ( M.y * _N /*+ self->r[i]->x * self->v[i]->z*/) / self->r[i]->z;
			self->v[i]->y -= 0.5 * (-M.x * _N /*+ self->r[i]->y * self->v[i]->z*/) / self->r[i]->z;
		}
	}
}

static void simulation_normalize_anagular_momentum(Simulation *self) {
	Vec M;
	do {
		simulation_normalize_anagular_momentum_(self);
		M = angular_momentum(self->r, self->v, self->N);
	} while (fabs(M.x) > 1e-8 && fabs(M.y) > 1e-8 && fabs(M.z) > 1e-8);
}

static void simulation_set_positions(Simulation *self) {
	int i, j, k, p;
	unsigned L = self->L;
	double c = (L-1)/2.0;
	double m = sqrt(2.0) * pow(2.0, 1.0/6.0);

	for (i = 0; i < L; ++i) {
		for (j = 0; j < L; ++j) {
			for (k = 0; k < L; ++k) {
				p = (i*L*L + j*L + k);
				self->r[p]->x = (i - c) * m + RD;
				self->r[p]->y = (j - c) * m + RD;
				self->r[p]->z = (k - c) * m + RD;
			}
		}
	}
	for (i = 0; i < L-1; ++i) {
		for (j = 0; j < L-1; ++j) {
			for (k = 0; k < L; ++k) {
				p = 3 * (i*L*(L-1) + j*L + k) + L*L*L;

				self->r[ p ]->x = (i - c + 0.5) * m + RD;
				self->r[ p ]->y = (j - c + 0.5) * m + RD;
				self->r[ p ]->z = (k - c + 0.0) * m + RD;

				self->r[p+1]->x = (i - c + 0.5) * m + RD;
				self->r[p+1]->y = (k - c + 0.0) * m + RD;
				self->r[p+1]->z = (j - c + 0.5) * m + RD;

				self->r[p+2]->x = (k - c + 0.0) * m + RD;
				self->r[p+2]->y = (i - c + 0.5) * m + RD;
				self->r[p+2]->z = (j - c + 0.5) * m + RD;
			}
		}
	}
}

void simulation_set_temp(Simulation *self, double T) {
	int i;
	double sigma = sqrt(T);
	for (i = 0; i < self->N; ++i) {
		self->v[i]->x = gsl_ran_gaussian(self->rand, sigma);
		self->v[i]->y = gsl_ran_gaussian(self->rand, sigma);
		self->v[i]->z = gsl_ran_gaussian(self->rand, sigma);
	}
	simulation_normalize_velocities(self);
	if (self->mean_displacement == 0.0) {
		simulation_normalize_anagular_momentum(self);
	}
}

Simulation *simulation_init(int L, double h, unsigned nsm, unsigned nss, unsigned nsw,
                            double mean_displacement, double scale, double ti, double tf, double ts) {
	Simulation *self = malloc(sizeof(Simulation));
	self->N = L*L*L + 3*L*(L-1)*(L-1);
	self->L = L;
	self->mean_displacement = mean_displacement;
	self->nsteps_measurement = nsm;
	self->nsteps_scaling = nss;
	self->nsteps_wait = nsw;
	self->scale = scale;
	self->ti = ti;
	self->tf = tf;
	self->ts = ts;
	self->h = h;

	// Initialise random number generator
	self->rand = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(self->rand, time(NULL));

	// Initialise positions (FCC)
	self->r = space_new(self->N);
	simulation_set_positions(self);

	// Initialise velocities
	self->v = space_new(self->N);
	simulation_set_temp(self, self->ti);

	// Initialise forces
	self->f = force(self->r, self->N);

	// Initialise history
	self->history_kinetic = g_queue_new();
	self->history_potential = g_queue_new();
	self->history_radius = g_queue_new();

	return self;
}

void simulation_free(Simulation *self) {
	if (self) {
		space_free(self->r, self->N);
		space_free(self->v, self->N);
		space_free(self->f, self->N);
		gsl_rng_free(self->rand);
	//	g_queue_free_full(self->history_kinetic, free); // since glib-2.32
	//	g_queue_free_full(self->history_potential, free);
	//	g_queue_free_full(self->history_radius, free);
		free(self);
		self = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static double simulation_system_radius(Simulation *self) {
	int i;
	double radius = 0.0, result = 0.0;
	for (i = 0; i < self->N; ++i) {
		radius = vec_len(self->r[i]);
		if (result < radius) {
			result = radius;
		}
	}
	return result;
}

void simulation_step(Simulation *self) {
	Space *r_new = verlet_r(self->r, self->v, self->f, self->h, self->N);
	Space *f_new = force(r_new, self->N);
	Space *v_new = verlet_v(self->v, f_new, self->f, self->h, self->N);
	space_free(self->r, self->N);
	space_free(self->v, self->N);
	space_free(self->f, self->N);
	self->r = r_new;
	self->v = v_new;
	self->f = f_new;
}

void simulation_step_scaling(Simulation *self, double scale) {
	int i;
	simulation_step(self);
	for (i = 0; i < self->N; ++i) {
		self->v[i]->x *= scale;
		self->v[i]->y *= scale;
		self->v[i]->z *= scale;
	}
}

void simulation_step_measure(Simulation *self, double scale) {
	static int n = 1;
	static double K = 0.0;
	static double U = 0.0;
	static double R = 0.0;

	double *k = malloc(sizeof(double));
	double *u = malloc(sizeof(double));
	double *r = malloc(sizeof(double));

	simulation_step_scaling(self, scale);

	*k = kinetic(self->v, self->N);
	*u = potential(self->r, self->N);
	*r = simulation_system_radius(self);

	K += *k; U += *u; R += *r;
	g_queue_push_head(self->history_kinetic, k);
	g_queue_push_head(self->history_potential, u);
	g_queue_push_head(self->history_radius, r);

	if (n > 1000) {
		k = g_queue_pop_tail(self->history_kinetic);
		u = g_queue_pop_tail(self->history_potential);
		r = g_queue_pop_tail(self->history_radius);
		K -= *k; U -= *u; R -= *r;
		free(k);
		free(u);
		free(r);
	} else {
		n += 1;
	}

	self->K = K / n;
	self->U = U / n;
	self->R = R / n;
	self->T = 2.0 * self->K / (3.0 * self->N);
}

/*
static double simulation_maxwell_transform(double v, double scale, double t_log_scale) {
	return sqrt(t_log_scale + v*v/scale) * (v < 0.0 ? -1.0 : 1.0);
}

void simulation_step_growing(Simulation *self, double scale) {
	int i;
	double t_log_scale = self->T * log(scale);
	simulation_step(self);
	for (i = 0; i < self->N; ++i) {
		self->v[i]->x = simulation_maxwell_transform(self->v[i]->x, scale, t_log_scale);
		self->v[i]->y = simulation_maxwell_transform(self->v[i]->y, scale, t_log_scale);
		self->v[i]->z = simulation_maxwell_transform(self->v[i]->z, scale, t_log_scale);
	}
}
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void simulation_measurement(Simulation *self, unsigned steps) {
	int i;
	double T, K, U, R;
	T = K = U = R = 0.0;
	for (i = 0; i < steps; ++i) {
		simulation_step(self);
		T += temperature(self->v, self->N);
		K += kinetic(self->v, self->N);
		U += potential(self->r, self->N);
		R += simulation_system_radius(self);
	}
	self->T = T / steps;
	self->K = K / steps;
	self->U = U / steps;
	self->R = R / steps;
}

static void simulation_scaling(Simulation *self, SimScaleStepFunc scale_step, double scale) {
	int i, j;
	for (i = 0; i < self->nsteps_scaling; ++i) {
		scale_step(self, scale);
		for (j = 0; j < self->nsteps_wait; ++j) {
			simulation_step(self);
		}
	}
}

void simulation_go_scaling(Simulation *self) {
	Vec M;
	int i, j, k;
	simulation_measurement(self, self->nsteps_measurement);

	printf("\n# heating\n"); fflush(stdout);
	for (i = 0; i < 10; ++i) {
		simulation_scaling(self, simulation_step_scaling, self->scale);//1.0 + (self->scale-1.0)*self->t0/self->T);
		simulation_measurement(self, 0.1 * self->nsteps_measurement * self->T);
		M = angular_momentum(self->r, self->v, self->N);
		printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", self->T, self->K, self->U,
		       self->K + self->U, self->R, M.x, M.y, M.z); fflush(stdout);
		if (self->T > 0.3) {
			break;
		}
	}

	simulation_measurement(self, self->nsteps_measurement * self->T);

	printf("\n\n# cooling\n"); fflush(stdout);
	for (i = 0; i < 100; ++i) {
		simulation_scaling(self, simulation_step_scaling, 2.0 - self->scale);
		simulation_measurement(self, 0.1 * self->nsteps_measurement * self->T);
		M = angular_momentum(self->r, self->v, self->N);
		printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", self->T, self->K, self->U,
		       self->K + self->U, self->R, M.x, M.y, M.z); fflush(stdout);
		if (self->T < self->ti) {
			break;
		}
	}

	simulation_measurement(self, self->nsteps_measurement * self->T);

	printf("\n\n# heating\n"); fflush(stdout);
	for (i = 0; i < 3000; ++i) {
		for (j = 0; j < self->nsteps_scaling * self->ti / self->T; ++j) {
			simulation_step_scaling(self, self->scale);
			for (k = 0; k < self->nsteps_wait; ++k) {
				simulation_step(self);
			}
		}
		simulation_measurement(self, self->nsteps_measurement * self->T);
		simulation_measurement(self, self->nsteps_measurement * self->T);
		M = angular_momentum(self->r, self->v, self->N);
		printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", self->T, self->K, self->U,
		       self->K + self->U, self->R, M.x, M.y, M.z); fflush(stdout);
		if (self->T > self->tf) {
			break;
		}
	}
	printf("\n# EOF\n");
}

void simulation_go_setting(Simulation *self) {
	double t;
	Vec M;
	for (t = self->ti; t < self->tf; t += self->ts) {
		simulation_set_positions(self);
		simulation_set_temp(self, t);
		simulation_measurement(self, self->nsteps_measurement);
		simulation_measurement(self, self->nsteps_measurement);
		M = angular_momentum(self->r, self->v, self->N);
		printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", self->T, self->K, self->U,
		       self->K + self->U, self->R, M.x, M.y, M.z, t); fflush(stdout);
	}
	printf("\n# EOF\n");
}

void simulation_go_measuring(Simulation *self) {
	Vec M;
	int i, j = 0;
	simulation_set_temp(self, self->ti);
	simulation_step(self);
	for (i = 0; i < self->nsteps_measurement; ++i) {
		simulation_step_measure(self, 1.0);
		if (j > self->nsteps_wait) {
			M = angular_momentum(self->r, self->v, self->N);
			printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", self->T, self->K, self->U,
				   self->K + self->U, self->R, M.x, M.y, M.z); fflush(stdout);
			j = 0;
		} else {
			j += 1;
		}
	}
	printf("\n# EOF\n");
}
