#ifndef __GUI_H__
#define __GUI_H__

#include "simulation.h"

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <GL/glut.h>

void gui_build(Simulation *);
void gui_destroy();

#endif

