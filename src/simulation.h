#ifndef __SIMULATION_H__
#define __SIMULATION_H__

#include "tools.h"

#include <gsl/gsl_rng.h>
#include <glib.h>

typedef struct {
	unsigned N; // Number of particles
	unsigned L;
	Space *r, *v, *f; // Positions, velocities and forces
	double scale;
	double ti, tf, ts;
	double mean_displacement;

	unsigned nsteps_measurement; // Number of steps to take average of measured quantities
	unsigned nsteps_scaling; // Number of steps to take while changing temperature
	unsigned nsteps_wait; // Number of steps between scaling velocities

	double T; // Temperature measured by simulation_measurement()
	double K; // Total energy measured by simulation_measurement()
	double U; // Potential energy measured by simulation_measurement()
	double R; // System radius measured by simulation_measurement()

	GQueue *history_kinetic;
	GQueue *history_potential;
	GQueue *history_radius;

	gsl_rng *rand; // Random number generator
	double h; // Step
} Simulation;

typedef void (*SimScaleStepFunc) (Simulation *, double);

Simulation *simulation_init(int,       // Length
                            double,    // Step
                            unsigned nsteps_mesurement,
                            unsigned nsteps_scaling,
                            unsigned nsteps_wait,
                            double mean_displacement,
                            double scale,
                            double ti,
                            double tf,
                            double ts);


void simulation_free(Simulation *);

void simulation_step(Simulation *);
void simulation_step_scaling(Simulation *, double);
void simulation_step_measure(Simulation *, double);
//void simulation_step_growing(Simulation *, double);

void simulation_go_scaling(Simulation *);
void simulation_go_setting(Simulation *);
void simulation_go_measuring(Simulation *);

#endif
