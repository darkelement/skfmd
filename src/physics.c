#include "physics.h"
#include <math.h>

#include <stdio.h>
#include <stdlib.h>

/**
***  Lennard-Jones potential:
***
***            /      12       6 \
***    _       | / s \    / s \  |
***  U(r) = 4e | | - |  - | - |  |
***            | \ r /    \ r /  |
***            \                 /
***
***
***     _     d   _        /  6  -8      12  -14 \
***  F (r) = -- U(r) = 24e | s  r   - 2 s   r    | r
***   i      dr            \                     /  i
***            i
***
***
***  _    /              \
***  r =  | r  , r  , r  |
***       \  x    y    z /
***
***
***         .-------------,
***        / 2    2    2
***  r =  / r  + r  + r
***      V   x    y    z
***
**/

double norm(double x, double y, double z) {
	return sqrt(x*x + y*y + z*z);
}

Space *force(Space *r, unsigned N) {
	int i, j;
	Space *result = space_new(N);

	for (i = 0; i < N; ++i) {
		#pragma omp parallel for
		for (j = 0; j < N; ++j) {
			if (i == j) continue;

			double d = vec_diff_len(r[i], r[j]);
			double f = 24.0 * (2 * pow(d, -14.0) - pow(d, -8.0));

		//	#pragma omp atomic
		//	result[i]->x -= f * (r[j]->x - r[i]->x);
		//	#pragma omp atomic
		//	result[i]->y -= f * (r[j]->y - r[i]->y);
		//	#pragma omp atomic
		//	result[i]->z -= f * (r[j]->z - r[i]->z);

		//	#pragma omp atomic
			result[j]->x += f * (r[j]->x - r[i]->x);
		//	#pragma omp atomic
			result[j]->y += f * (r[j]->y - r[i]->y);
		//	#pragma omp atomic
			result[j]->z += f * (r[j]->z - r[i]->z);
		}
	}
	return result;
}

double potential(Space *r, unsigned N) {
	int i, j;
	double d, result = 0.0;
	for (i = 0; i < N; ++i) {
		#pragma omp parallel for
		for (j = 0; j < i; ++j) {
			d = vec_diff_len(r[i], r[j]);
			result += pow(d, -12.0) - pow(d, -6.0);
		}
	}
	return 4.0 * result;
}

double kinetic(Space *v, unsigned N) {
	int i;
	double result = 0.0;
	for (i = 0; i < N; ++i) {
		result += v[i]->x * v[i]->x + v[i]->y * v[i]->y + v[i]->z * v[i]->z;
	}
	return 0.5 * result;
}

double temperature(Space *v, unsigned N) {
	return 2 * kinetic(v, N) / (3 * N);
}

Vec angular_momentum(Space *r, Space *v, unsigned N) {
	int i;
	Vec result = {0.0};
	for (i = 0; i < N; ++i) {
		result.x += r[i]->y * v[i]->z - r[i]->z * v[i]->y;
		result.y += r[i]->z * v[i]->x - r[i]->x * v[i]->z;
		result.z += r[i]->x * v[i]->y - r[i]->y * v[i]->x;
	}
	return result;
}

Vec mean_velocity(Space *v, unsigned N) {
	int i;
	Vec result = {0};
	for (i = 0; i < N; ++i) {
		result.x += v[i]->x;
		result.y += v[i]->y;
		result.z += v[i]->z;
	}
	result.x /= N;
	result.y /= N;
	result.z /= N;
	return result;
}

Histogram *correlation(Space *r, unsigned N) {
	Histogram *result;
	unsigned i, size = 0.5 * N;
	double max_len, *len = malloc(N * sizeof(double));

	for (i = 0; i < N; ++i) {
		len[i] = vec_len(r[i]);
		if (len[i] > max_len) {
			max_len = len[i];
		}
	}

	result = histogram_new(size, max_len / size);
	for (i = 0; i < N; ++i) {
		++result->v[(int) (len[i] / max_len * (size-1))];
	}

	free(len);
	return result;
}
