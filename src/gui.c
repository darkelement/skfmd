#include "gui.h"
#include "physics.h"

#include <stdlib.h>
#include <math.h>

#include <gdk/gdkkeysyms.h>
#include <pthread.h>
#include <GL/gl.h>

#ifndef NOPLOTS
	#include <gtkextra/gtkextra.h>
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VARIABLES

#define LABEL_BUTTON_START "Start\nsimulation"
#define LABEL_BUTTON_STOP  "Stop\nsimulation"

#define PADDING 2
#define SPACING 2

static GtkWindow *window;
static GtkBox *box_main, *box_side, *box_plots;
static GtkButton *button_start_stop;
static GtkDrawingArea *darea;
static GtkLabel *label_values, *label_angles, *label_step;
static GtkSpinButton *spin_vscale;

#ifndef NOPLOTS
	static GtkPlotData *dataset_kinetic;
	static GtkPlot *plot_kinetic;
	static GtkPlotCanvas *plot_kinetic_canvas;

	static GtkPlotData *dataset_potential;
	static GtkPlot *plot_potential;
	static GtkPlotCanvas *plot_potential_canvas;

	static GtkPlotData *dataset_correlation;
	static GtkPlot *plot_correlation;
	static GtkPlotCanvas *plot_correlation_canvas;
#endif

static double *data_kinetic;
static double *data_potential;
static double *data_time;

static Simulation *sim;
static gboolean sim_active;
static Space *positions_copy;
static Space *velocities_copy;

static int theta = 0;
static int phi = 0;
static double scale = 1.0;

static GdkGLConfig *glconfig;

static GLfloat LightPos[] =   {10.0, 10.0, 10.0, 0.0};
static GLfloat LightColor[] = { 0.3,  0.3,  0.7, 1.0};

static pthread_t thread_simulation;
#ifndef NOPLOTS
	static pthread_t thread_plots;
#endif
static pthread_mutex_t mutex_coping_positions = PTHREAD_MUTEX_INITIALIZER;

static int nsteps = 0;
static const int plot_points = 30;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TOOLS

void gui_set_values(double T, double K, double U) {
	gtk_label_set_markup(label_values, g_strdup_printf("<b>Temperature:</b>\n%f\n\n"
	    "<b>Kinetic energy:</b>\n%f\n\n<b>Potential energy:</b>\n%f\n\n", T, K, U));
}

void gui_set_angles(int phi, int theta) {
	gtk_label_set_markup(label_angles, g_strdup_printf("<b>Phi:  </b> %i\n<b>Theta:</b> %i", phi, theta));
}

#ifndef NOPLOTS
	void gui_plot_energy() {
		int i;
		double max_kinetic = 0.0;
		double max_potential = -1e32;
		double min_potential = 0.0;
		double min_kinetic = 1e32;
		for (i = 0; i < plot_points - 1; ++i) {
			data_kinetic[i] = data_kinetic[i+1];
			data_potential[i] = data_potential[i+1];
			data_time[i] = data_time[i+1];
			if (max_kinetic < data_kinetic[i]) {
				max_kinetic = data_kinetic[i];
			}
			if (max_potential < data_potential[i]) {
				max_potential = data_potential[i];
			}
			if (min_kinetic > data_kinetic[i]) {
				min_kinetic = data_kinetic[i];
			}
			if (min_potential > data_potential[i]) {
				min_potential = data_potential[i];
			}
		}
		data_kinetic[plot_points-1] = sim->K;
		data_potential[plot_points-1] = sim->U;
		data_time[plot_points-1] = nsteps;

		gtk_plot_set_range(GTK_PLOT(plot_kinetic), data_time[0], data_time[plot_points-1], min_kinetic, max_kinetic);
		gtk_plot_axis_set_ticks(gtk_plot_get_axis(plot_kinetic, GTK_PLOT_AXIS_LEFT), (max_kinetic-min_kinetic)/5.0, 5);
		gtk_plot_axis_set_ticks(gtk_plot_get_axis(plot_kinetic, GTK_PLOT_AXIS_BOTTOM), data_time[plot_points-1]/5.0, 5);

		gtk_plot_data_set_points(dataset_kinetic, data_time, data_kinetic, NULL, NULL, plot_points);
		gtk_plot_add_data(plot_kinetic, dataset_kinetic);
		gtk_widget_queue_draw(GTK_WIDGET(plot_kinetic_canvas));

		gtk_plot_set_range(GTK_PLOT(plot_potential), data_time[0], data_time[plot_points-1], min_potential, max_potential);
		gtk_plot_axis_set_ticks(gtk_plot_get_axis(plot_potential, GTK_PLOT_AXIS_LEFT), fabs((max_potential-min_potential)/5.0), 5);
		gtk_plot_axis_set_ticks(gtk_plot_get_axis(plot_potential, GTK_PLOT_AXIS_BOTTOM), data_time[plot_points-1]/5.0, 5);

		gtk_plot_data_set_points(dataset_potential, data_time, data_potential, NULL, NULL, plot_points);
		gtk_plot_add_data(plot_potential, dataset_potential);
		gtk_widget_queue_draw(GTK_WIDGET(plot_potential_canvas));
	}

	void gui_plot_correlation() {
		int i;
		static Histogram *corr = NULL;
		static double *x = NULL;
		histogram_free(corr);
		free(x);

		corr = correlation(positions_copy, sim->N);
		x = malloc(corr->size * sizeof(double));
		for (i = 0; i < corr->size; ++i) {
			x[i] = i * corr->h;
		}

		gtk_plot_set_range(GTK_PLOT(plot_correlation), 0.0, x[corr->size-1], 0.0, histogram_get_max(corr));
		gtk_plot_axis_set_ticks(gtk_plot_get_axis(plot_correlation, GTK_PLOT_AXIS_LEFT), histogram_get_max(corr)/5.0, 5);
		gtk_plot_axis_set_ticks(gtk_plot_get_axis(plot_correlation, GTK_PLOT_AXIS_BOTTOM), x[corr->size-1]/5.0, 5);

		gtk_plot_data_set_points(dataset_correlation, x, corr->v, NULL, NULL, corr->size);
		gtk_plot_add_data(plot_correlation, dataset_correlation);
		gtk_widget_queue_draw(GTK_WIDGET(plot_correlation_canvas));
	}

	static gpointer gui_plots_(gpointer data) {
		gui_plot_correlation();
		gui_plot_energy();
		gui_set_values(2.0 * sim->K / (3.0 * sim->N), sim->K, sim->U);
		return NULL;
	}
#endif

int gui_plots(gpointer data) {
	if (sim_active) {
		#ifndef NOPLOTS
			pthread_create(&thread_plots, NULL, gui_plots_, NULL);
		#endif
		gtk_label_set_markup(label_step, g_strdup_printf("<b>Steps:</b>\n%d", nsteps));
	}
	return TRUE;
}

gpointer gui_draw_atoms(gpointer data) {
	// Prepare
	GdkGLContext *glcontext = gtk_widget_get_gl_context(GTK_WIDGET(darea));
	GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable(GTK_WIDGET(darea));
	if (!gdk_gl_drawable_gl_begin(gldrawable, glcontext)) {
		fprintf(stderr, "ERROR: Could not initialize gl drawable!");
		return NULL;
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,  LightColor);
	glMaterialfv(GL_LIGHT0, GL_DIFFUSE,  LightColor);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPos);

	// Rotate
	glPushMatrix();
	glScalef(scale, scale, scale);
	glRotatef(theta, 1, 0, 0);
	glRotatef(phi,   0, 1, 0);

	// Draw
	pthread_mutex_lock(&mutex_coping_positions);

	int i;
	for (i = 0; i < sim->N; ++i) {
		glTranslatef(positions_copy[i]->x, positions_copy[i]->y, positions_copy[i]->z);
		glutSolidSphere(0.2, 8, 8);
		glTranslatef(-positions_copy[i]->x, -positions_copy[i]->y, -positions_copy[i]->z);
	}

	pthread_mutex_unlock(&mutex_coping_positions);

	glPopMatrix();

	// Flush
	if (gdk_gl_drawable_is_double_buffered(gldrawable)) {
		gdk_gl_drawable_swap_buffers(gldrawable);
	} else {
		glFlush();
	}
	gdk_gl_drawable_gl_end(gldrawable);

	gui_set_angles(phi, theta);
	return NULL;
}

gpointer gui_simulation(gpointer data) {
	Vec M;
	while (sim_active) {
		simulation_step_measure(sim, gtk_spin_button_get_value(spin_vscale));
		pthread_mutex_lock(&mutex_coping_positions);
		space_copy(sim->r, positions_copy, sim->N);
		space_copy(sim->v, velocities_copy, sim->N);
		pthread_mutex_unlock(&mutex_coping_positions);
		gtk_widget_queue_draw(GTK_WIDGET(darea));

		if (nsteps % 100 == 0) {
			M = angular_momentum(positions_copy, velocities_copy, sim->N);
			printf("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", sim->T, sim->K, sim->U,
		           sim->K + sim->U, sim->R, M.x, M.y, M.z); fflush(stdout);
		}

		nsteps += 1;
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// WINDOW EVENTS

static gboolean delete_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
	if (sim_active) {
		sim_active = FALSE;
		pthread_join(thread_simulation, NULL);
	}
	return FALSE;
}

static void destroy(GtkWidget *widget, gpointer data) {
	gtk_main_quit();
}

static void start_stop_sim(GtkWidget *widget, gpointer data) {
	sim_active = !sim_active;
	gtk_button_set_label(button_start_stop, sim_active ? LABEL_BUTTON_STOP : LABEL_BUTTON_START);
	if (sim_active) {
		pthread_create(&thread_simulation, NULL, gui_simulation, NULL);
	}
}

static gboolean key_press(GtkWidget *widget, GdkEventKey *event, gpointer data) {
	switch (event->keyval) {
		case GDK_KEY_Right: phi =   (phi+1)     % 360; break;
		case GDK_KEY_Left:  phi =   (phi+359)   % 360; break;
		case GDK_KEY_Down:  theta = (theta+1)   % 360; break;
		case GDK_KEY_Up:    theta = (theta+359) % 360; break;
		case GDK_KEY_plus:  case GDK_KEY_KP_Add:      scale += 0.1; break;
		case GDK_KEY_minus: case GDK_KEY_KP_Subtract: scale -= 0.1; break;
		default: return FALSE;
	}
	gtk_widget_queue_draw(GTK_WIDGET(darea));
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DRAWING AREA EVENTS

static gboolean darea_realize(GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext = gtk_widget_get_gl_context(widget);
	GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable(widget);
	if (!gdk_gl_drawable_gl_begin(gldrawable, glcontext)) {
		fprintf(stderr, "ERROR: Could not initialize gl drawable!");
		return FALSE;
	}

	glLoadIdentity();

	int h, w, m;
	w = widget->allocation.width;
	h = widget->allocation.height;
	m = w < h ? w : h;
	glViewport(0, 0, m, m);
	glOrtho(-10, 10, -10, 10, -20050, 10000);

	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glEnable(GL_COLOR_MATERIAL);

	gdk_gl_drawable_gl_end(gldrawable);
	return TRUE;
}

static gboolean darea_expose(GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	gui_draw_atoms(widget);
	return TRUE;
}

static gboolean darea_configure(GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	return darea_realize(widget, event, data);
}

#ifndef NOPLOTS
	static int plot_expose(GtkPlotCanvas *canvas, GdkEventConfigure *event, gpointer user_data) {
		int width = GTK_WIDGET(canvas)->allocation.width;
		int height = GTK_WIDGET(canvas)->allocation.height;
		if (width != canvas->width || height != canvas->height) {
			gtk_plot_canvas_set_size(canvas, width, height);
		}
		gtk_plot_canvas_paint(canvas);
		return 0;
	}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BUILD GUI

#ifndef NOPLOTS
	void gui_create_plot(GtkPlotData **dataset, GtkPlotCanvas **plot_canvas, GtkPlot **plot, char *x_title, char *y_title) {
		GdkColor color;
		GtkPlotAxis *x_axis, *y_axis;
		*dataset = GTK_PLOT_DATA(gtk_plot_data_new());
		*plot_canvas = GTK_PLOT_CANVAS(gtk_plot_canvas_new(400, 200, 1.0));
		*plot = GTK_PLOT(gtk_plot_new(NULL));

		gtk_plot_canvas_put_child(*plot_canvas, gtk_plot_canvas_plot_new(*plot), 0.15, 0.05, 0.95, 0.85);

		// Setup the axis
		gtk_plot_set_legends_border(*plot, (GtkPlotBorderStyle) 0, 0);
		gtk_plot_axis_set_visible(gtk_plot_get_axis(*plot, GTK_PLOT_AXIS_TOP), FALSE);
		gtk_plot_axis_set_visible(gtk_plot_get_axis(*plot, GTK_PLOT_AXIS_RIGHT), FALSE);

		x_axis = gtk_plot_get_axis(*plot, GTK_PLOT_AXIS_BOTTOM);
		y_axis = gtk_plot_get_axis(*plot, GTK_PLOT_AXIS_LEFT);

		gtk_plot_axis_set_title(x_axis, x_title);
		gtk_plot_axis_set_title(y_axis, y_title);

		// Setup data set
		gdk_color_parse("red", &color);
		gtk_plot_data_set_symbol(*dataset, GTK_PLOT_SYMBOL_EMPTY, GTK_PLOT_SYMBOL_EMPTY, 10, 2, &color, &color);
		gtk_plot_data_set_line_attributes(*dataset, GTK_PLOT_LINE_SOLID, (GdkCapStyle) 0, (GdkJoinStyle) 0, 2, &color);
		gtk_plot_data_set_connector(*dataset, GTK_PLOT_CONNECT_STRAIGHT);
		gtk_plot_data_hide_legend(*dataset);
		gtk_widget_show(GTK_WIDGET(*dataset));

		// Connect events
		g_signal_connect(G_OBJECT(*plot_canvas), "expose-event", G_CALLBACK(&plot_expose), NULL);
	}
#endif

void gui_build(Simulation *simulation) {
	sim = simulation;
	sim_active = FALSE;
	positions_copy = space_new(sim->N);
	velocities_copy = space_new(sim->N);
	space_copy(sim->r, positions_copy, sim->N);
	space_copy(sim->v, velocities_copy, sim->N);
	data_kinetic = calloc(plot_points, sizeof(double));
	data_potential = calloc(plot_points, sizeof(double));
	data_time = calloc(plot_points, sizeof(double));

	// Create widgets
	window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
	box_main = GTK_BOX(gtk_hbox_new(FALSE, SPACING));
	box_side = GTK_BOX(gtk_vbox_new(FALSE, SPACING));
	#ifndef NOPLOTS
		box_plots = GTK_BOX(gtk_vbox_new(FALSE, SPACING));
	#endif

	darea = GTK_DRAWING_AREA(gtk_drawing_area_new());
	gtk_widget_add_events(GTK_WIDGET(darea), GDK_EXPOSURE_MASK | GDK_KEY_PRESS_MASK | GDK_BUTTON_PRESS_MASK);
	gtk_widget_set_can_focus(GTK_WIDGET(darea), TRUE);

	button_start_stop = GTK_BUTTON(gtk_button_new_with_label(LABEL_BUTTON_START));
	label_values = GTK_LABEL(gtk_label_new(""));
	label_angles = GTK_LABEL(gtk_label_new(""));
	label_step = GTK_LABEL(gtk_label_new(""));
	spin_vscale = GTK_SPIN_BUTTON(gtk_spin_button_new_with_range(0.99000, 1.00100, 0.000001));

	gtk_label_set_use_markup(label_values, TRUE);
	gtk_label_set_use_markup(label_angles, TRUE);
	gtk_label_set_use_markup(label_step, TRUE);
	gtk_label_set_line_wrap_mode(label_values, TRUE);
	gtk_label_set_line_wrap_mode(label_angles, TRUE);
	gtk_label_set_line_wrap_mode(label_step, TRUE);
	gtk_spin_button_set_value(spin_vscale, 1.0);

	// Plots
	#ifndef NOPLOTS
		gui_create_plot(&dataset_kinetic, &plot_kinetic_canvas, &plot_kinetic, "time", "kinetic energy");
		gui_create_plot(&dataset_potential, &plot_potential_canvas, &plot_potential, "time", "potential energy");
		gui_plot_energy();

		gui_create_plot(&dataset_correlation, &plot_correlation_canvas, &plot_correlation, "distance", "number of atoms");
		gui_plot_correlation();
	#endif

	// Pack widgets
	gtk_box_pack_start(box_side, GTK_WIDGET(button_start_stop), TRUE, TRUE, PADDING);
	gtk_box_pack_start(box_side, GTK_WIDGET(label_values), TRUE, TRUE, PADDING);
	gtk_box_pack_start(box_side, GTK_WIDGET(label_angles), TRUE, TRUE, PADDING);
	gtk_box_pack_start(box_side, GTK_WIDGET(spin_vscale), TRUE, TRUE, PADDING);
	gtk_box_pack_start(box_side, GTK_WIDGET(label_step), TRUE, TRUE, PADDING);


	#ifndef NOPLOTS
		gtk_box_pack_start(box_plots, GTK_WIDGET(plot_kinetic_canvas), TRUE, TRUE, PADDING);
		gtk_box_pack_start(box_plots, GTK_WIDGET(plot_potential_canvas), TRUE, TRUE, PADDING);
		gtk_box_pack_start(box_plots, GTK_WIDGET(plot_correlation_canvas), TRUE, TRUE, PADDING);
	#endif

	gtk_box_pack_start(box_main, GTK_WIDGET(box_side), FALSE, FALSE, PADDING);
	gtk_box_pack_start(box_main, GTK_WIDGET(darea), TRUE, TRUE, PADDING);
	gtk_box_pack_start(box_main, GTK_WIDGET(box_plots), FALSE, FALSE, PADDING);

	gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(box_main));

	// Prepare GL
	glconfig = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGB | GDK_GL_MODE_DEPTH | GDK_GL_MODE_DOUBLE);
	if (!glconfig) {
		g_assert_not_reached();
	}
	if (!gtk_widget_set_gl_capability(GTK_WIDGET(darea), glconfig, NULL, TRUE, GDK_GL_RGBA_TYPE)) {
		g_assert_not_reached();
	}

	// Show all
	gtk_widget_show_all(GTK_WIDGET(window));
	gtk_widget_grab_focus(GTK_WIDGET(darea));

	// Connect signals
	g_signal_connect(window, "delete-event", G_CALLBACK(delete_event), NULL);
	g_signal_connect(window, "destroy", G_CALLBACK(destroy), NULL);

	g_signal_connect(button_start_stop, "clicked", G_CALLBACK(start_stop_sim), NULL);

	g_signal_connect(darea, "realize", G_CALLBACK(darea_realize), NULL);
	g_signal_connect(darea, "configure-event", G_CALLBACK(darea_configure), NULL);
	g_signal_connect(darea, "expose-event", G_CALLBACK(darea_expose), NULL);
	g_signal_connect(darea, "key-press-event", G_CALLBACK(key_press), NULL);
	g_signal_connect(darea, "button-press-event", G_CALLBACK(gtk_widget_grab_focus), darea);

	g_timeout_add(5000, gui_plots, NULL);
}

void gui_destroy() {
	space_free(positions_copy, sim->N);
	space_free(velocities_copy, sim->N);
	free(data_kinetic);
	free(data_potential);
	free(data_time);
}
