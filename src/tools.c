#include "tools.h"

#include <malloc.h>
#include <math.h>

// VECTOR

Vec *vec_new() {
	Vec *self = malloc(sizeof(Vec));
	self->x = self->y = self->z = 0.0;
	return self;
}

void vec_free(Vec *self) {
	if (self) {
		free(self);
		self = NULL;
	}
}

double vec_len(Vec *self) {
	return sqrt(self->x*self->x + self->y*self->y + self->z*self->z);
}

double vec_diff_len(Vec *self, Vec *other) {
	Vec v;
	v.x = self->x - other->x;
	v.y = self->y - other->y;
	v.z = self->z - other->z;
	return vec_len(&v);
}

// ARRAY OF VECTORS

Space *space_new(int size) {
	int i;
	Space *self = malloc(size * sizeof(Vec *));
	for (i = 0; i < size; ++i) {
		self[i] = vec_new();
	}
	return self;
}

void space_free(Space *self, int size) {
	if (self) {
		int i;
		for (i = 0; i < size; ++i) {
			vec_free(self[i]);
		}
		free(self);
		self = NULL;
	}
}

void space_copy(Space *self, Space *other, int size) {
	int i;
	for (i = 0; i < size; ++i) {
		other[i]->x = self[i]->x;
		other[i]->y = self[i]->y;
		other[i]->z = self[i]->z;
	}
}

// HISTOGRAM

Histogram *histogram_new(unsigned size, double h) {
	int i;
	Histogram *self = malloc(sizeof(Histogram));
	self->v = malloc(size * sizeof(double));
	for (i = 0; i < size; ++i) {
		self->v[i] = 0.0;
	}
	self->size = size;
	self->h = h;
	return self;
}

void histogram_free(Histogram *self) {
	if (self) {
		free(self->v);
		free(self);
		self = NULL;
	}
}

double histogram_get_max(Histogram *self) {
	int i;
	double result = 0.0;
	for (i = 0; i < self->size; ++i) {
		if (result < self->v[i]) {
			result = self->v[i];
		}
	}
	return result;
}
