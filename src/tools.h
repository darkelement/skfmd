#ifndef __TOOLS_H__
#define __TOOLS_H__

// VECTOR

typedef struct {
	double x, y, z;
} Vec;

Vec *vec_new();
void vec_free(Vec *);

double vec_len(Vec *);
double vec_diff_len(Vec *, Vec *);

// ARRAY OF VECTORS

typedef Vec *Space;

Space *space_new(int);
void space_free(Space *, int);
void space_copy(Space *, Space *, int);

// HISTOGRAM

typedef struct {
	double *v;
	unsigned size;
	double h;
} Histogram;

Histogram *histogram_new(unsigned, double);
void histogram_free(Histogram *);

double histogram_get_max(Histogram *);

#endif
